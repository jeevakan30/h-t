package com.flightapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.flightapp.model.Flights;

public interface FlightRepository extends JpaRepository<Flights, Integer>{
	
	public List<Flights> findAll();

}
