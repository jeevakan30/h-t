package com.flightappuser.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.flightappuser.model.BookingDetails;
import com.flightappuser.model.Flights;

public interface FlightRepository extends JpaRepository<Flights, Integer>{
	
	public List<Flights> findAll();
	
	@Query("SELECT u FROM Flights u WHERE u.fromPlace = ?1 and u.toPlace = ?2 and u.active = ?3")
	public List<Flights> getAllSearchFlights (String from, String to, String active);
	
	@Query("SELECT u FROM Flights u WHERE u.airline = ?1")
	public List<Flights> getAllByAirlineName (String airline);
}
