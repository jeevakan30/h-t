package com.flightappuser.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.flightappuser.model.BookingDetails;

public interface BookingDetailsRepository extends JpaRepository<BookingDetails, Integer>{
	
	public List<BookingDetails> findAll();
	
	@Query("SELECT u FROM BookingDetails u WHERE u.pnrNumber = ?1")
	public List<BookingDetails> getAllBookingStatusByPnr (String pnr);
	
	@Query("SELECT u FROM BookingDetails u WHERE u.userEmail = ?1")
	public List<BookingDetails> getAllBookingStatusByEmail (String email);

}