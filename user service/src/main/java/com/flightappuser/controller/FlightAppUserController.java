package com.flightappuser.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.flightappuser.exception.PNRNotFoundException;
import com.flightappuser.model.BookingDetails;
import com.flightappuser.model.Flights;
import com.flightappuser.service.BookingDetailsService;
import com.flightappuser.service.FlightService;


@RestController
@CrossOrigin
@RequestMapping("/v1/api/flight")
public class FlightAppUserController {

	@Autowired
	private BookingDetailsService bookingDetailsService;
	
	@Autowired
	private FlightService flightService;
	
	@GetMapping("/demo")
	public String getDemo() {	
		return "demo";
	}
	
	@GetMapping("/bookingHistory")
	public List<BookingDetails> getFlights() {	
		return bookingDetailsService.getAllBookingDetails();
	}
	
	@PostMapping("/bookTickets")
	public BookingDetails bookFlights(@RequestBody BookingDetails bookingDetails) {	
		return bookingDetailsService.bookTickets(bookingDetails);
	}
	
	@GetMapping("/airline/{pnr}")
	public List<BookingDetails>  pnrHistory(@PathVariable String pnr) throws PNRNotFoundException {	
		return bookingDetailsService.getAllBookingStatusByPnr(pnr);
	}
	
	@GetMapping("/airline/history/{email}")
	public List<BookingDetails> bookingHistory(@PathVariable String email) {	
		return bookingDetailsService.getAllBookingHistoryByEmail(email);
	}
	
	@PostMapping("/airline/searchFlights")
	public List<Flights> searchFlights(@RequestBody Flights searchFlights) {	
		return flightService.getSearchFlights(searchFlights);
	}
}
