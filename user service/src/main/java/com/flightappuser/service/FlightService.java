package com.flightappuser.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.flightappuser.model.Flights;
import com.flightappuser.repository.FlightRepository;

@Service
public class FlightService {

	@Autowired
	private FlightRepository repo;

	public List<Flights> getAllFlights() {
		return repo.findAll();
	}

	public Flights registerFlights(Flights flight) {
		return repo.save(flight);
	}
	public List<Flights> getAllByAirlineName(String Airline) {

		return repo.getAllByAirlineName(Airline);
	}

	public Flights getFlightsByid(int id) {
		Optional<Flights> optional = repo.findById(id);
		
		if(optional.isPresent()) {
			return optional.get();
		}else {
			throw new RuntimeException("Flight with "+id+ " not present");
		}
	}
	
	public void removeFlights(int id) {
		 repo.deleteById(id);
	}
	
	public List<Flights> getSearchFlights(Flights searchFlights) {	
		return repo.getAllSearchFlights(searchFlights.getFromPlace().toString(), searchFlights.getToPlace().toString(),"active");
	}
}
