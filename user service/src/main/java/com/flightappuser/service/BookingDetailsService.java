package com.flightappuser.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import com.flightappuser.exception.PNRNotFoundException;
import com.flightappuser.model.BookingDetails;
import com.flightappuser.model.Flights;
import com.flightappuser.repository.BookingDetailsRepository;
import com.flightappuser.repository.FlightRepository;

@Service
public class BookingDetailsService {
	@Autowired
	private BookingDetailsRepository bookingDetailsRepo;

	public List<BookingDetails> getAllBookingDetails() {

		System.out.println("Booking Details Service");
		return bookingDetailsRepo.findAll();
	}

	public List<BookingDetails> getAllBookingStatusByPnr (String pnr) throws PNRNotFoundException{
		System.out.println("pnr no" + pnr);
		List<BookingDetails> details = bookingDetailsRepo.getAllBookingStatusByPnr(pnr);
		if(details == null) {
			throw new PNRNotFoundException("Booking Details for PNR "+pnr+" not found");
		}else {
			return details;	
		}
	}

	public List<BookingDetails> getAllBookingHistoryByEmail (String email) {
		System.out.println("email " + email);
		return bookingDetailsRepo.getAllBookingStatusByEmail(email);		 
	}
	
	public BookingDetails bookTickets(BookingDetails bookingDetails) {
		System.out.println("BookingDetails" + bookingDetails);
		final String randomCode = UUID.randomUUID().toString().substring(0, 5);
		String pnr =  "PNR" + randomCode;
		bookingDetails.setPnrNumber(pnr);
		bookingDetails.setBookingStatus("Confirmed");
		return bookingDetailsRepo.save(bookingDetails);
	}
}
