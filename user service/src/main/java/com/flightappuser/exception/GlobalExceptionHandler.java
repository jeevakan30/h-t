package com.flightappuser.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(PNRNotFoundException.class)
	public ResponseEntity<String> handlePnrNotFoundException(PNRNotFoundException e){
		return new ResponseEntity<String>(e.getMessage(), HttpStatus.OK);
	}
	
}
