package com.flightappuser.exception;

public class PNRNotFoundException  extends Exception{
	
	public  PNRNotFoundException(){
		super();
	}
	public  PNRNotFoundException(String message){
		super(message);
	}
	public  PNRNotFoundException(Exception e){
		super(e);
	}
	public  PNRNotFoundException(String message, Exception e){
		super(message,e);
	}

}
