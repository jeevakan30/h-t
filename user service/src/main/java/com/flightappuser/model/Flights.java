package com.flightappuser.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Flights {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String airline;
	private String flightNumber;
	private String fromPlace;
	private String toPlace;
	private Timestamp startDateTime;
	private Timestamp endDateTime;
	private String scheduledTime; 
	private Integer businessSeats;
	private Integer nonBusinessSeats;
	private String ticketCost;
	private Integer totalTickets;
	private String meal;
	private String active;
	private String ticektCostBusiness;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAirline() {
		return airline;
	}
	public void setAirline(String airline) {
		this.airline = airline;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getFromPlace() {
		return fromPlace;
	}
	public void setFromPlace(String fromPlace) {
		this.fromPlace = fromPlace;
	}
	public String getToPlace() {
		return toPlace;
	}
	public void setToPlace(String toPlace) {
		this.toPlace = toPlace;
	}
	public Timestamp getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(Timestamp startDateTime) {
		this.startDateTime = startDateTime;
	}
	public Timestamp getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(Timestamp endDateTime) {
		this.endDateTime = endDateTime;
	}
	public String getScheduledTime() {
		return scheduledTime;
	}
	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}
	public Integer getBusinessSeats() {
		return businessSeats;
	}
	public void setBusinessSeats(Integer businessSeats) {
		this.businessSeats = businessSeats;
	}
	public Integer getNonBusinessSeats() {
		return nonBusinessSeats;
	}
	public void setNonBusinessSeats(Integer nonBusinessSeats) {
		this.nonBusinessSeats = nonBusinessSeats;
	}
	public String getTicketCost() {
		return ticketCost;
	}
	public void setTicketCost(String ticketCost) {
		this.ticketCost = ticketCost;
	}
	public Integer getTotalTickets() {
		return totalTickets;
	}
	public void setTotalTickets(Integer totalTickets) {
		this.totalTickets = totalTickets;
	}
	public String getMeal() {
		return meal;
	}
	public void setMeal(String meal) {
		this.meal = meal;
	}
	
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getTicektCostBusiness() {
		return ticektCostBusiness;
	}
	public void setTicektCostBusiness(String ticektCostBusiness) {
		this.ticektCostBusiness = ticektCostBusiness;
	}
	@Override
	public String toString() {
		return "Flights [id=" + id + ", airline=" + airline + ", flightNumber=" + flightNumber + ", fromPlace="
				+ fromPlace + ", toPlace=" + toPlace + ", startDateTime=" + startDateTime + ", endDateTime="
				+ endDateTime + ", scheduledTime=" + scheduledTime + ", businessSeats=" + businessSeats
				+ ", nonBusinessSeats=" + nonBusinessSeats + ", ticketCost=" + ticketCost + ", totalTickets="
				+ totalTickets + ", meal=" + meal + ", active=" + active + ", ticektCostBusiness=" + ticektCostBusiness
				+ ", getId()=" + getId() + ", getAirline()=" + getAirline() + ", getFlightNumber()=" + getFlightNumber()
				+ ", getFromPlace()=" + getFromPlace() + ", getToPlace()=" + getToPlace() + ", getStartDateTime()="
				+ getStartDateTime() + ", getEndDateTime()=" + getEndDateTime() + ", getScheduledTime()="
				+ getScheduledTime() + ", getBusinessSeats()=" + getBusinessSeats() + ", getNonBusinessSeats()="
				+ getNonBusinessSeats() + ", getTicketCost()=" + getTicketCost() + ", getTotalTickets()="
				+ getTotalTickets() + ", getMeal()=" + getMeal() + ", getActive()=" + getActive()
				+ ", getTicektCostBusiness()=" + getTicektCostBusiness() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	
	
}
