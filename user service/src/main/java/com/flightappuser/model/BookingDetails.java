package com.flightappuser.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BookingDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String flightNumber;
	private String userName;
	private String userEmail;
	private String pnrNumber;
	private String bookingStatus;
	private String seatsClass;
	private int noOfSeats;
	private String mealType;
	private Timestamp travellingDateTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getPnrNumber() {
		return pnrNumber;
	}
	public void setPnrNumber(String pnrNumber) {
		this.pnrNumber = pnrNumber;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public String getSeatsClass() {
		return seatsClass;
	}
	public void setSeatsClass(String seatsClass) {
		this.seatsClass = seatsClass;
	}
	public int getNoOfSeats() {
		return noOfSeats;
	}
	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
	public String getMealType() {
		return mealType;
	}
	public void setMealType(String mealType) {
		this.mealType = mealType;
	}
	public Timestamp gettravellingDateTime() {
		return travellingDateTime;
	}
	public void settravellingDateTime(Timestamp travellingDateTime) {
		this.travellingDateTime = travellingDateTime;
	}
	@Override
	public String toString() {
		return "BookingDetails [id=" + id + ", flightNumber=" + flightNumber + ", userName=" + userName + ", userEmail="
				+ userEmail + ", pnrNumber=" + pnrNumber + ", bookingStatus=" + bookingStatus + ", seatsClass="
				+ seatsClass + ", noOfSeats=" + noOfSeats + ", mealType=" + mealType + ", startDateTime="
				+ travellingDateTime + "]";
	}
	
	
}
