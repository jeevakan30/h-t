package com.flightapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.flightapp.model.Flights;
import com.flightapp.repository.FlightRepository;

@Service
public class FlightService {

	@Autowired
	private FlightRepository repo;

	public List<Flights> getAllFlights() {

		System.out.println("Flight Service");
		return repo.findAll();
	}

	public Flights registerFlights(Flights flight) {
		System.out.println("Register Flight" + flight);

		return repo.save(flight);
	}

	public void removeFlights(int id) {
		System.out.println("Remove Flight" + id);
		 repo.deleteById(id);
	}

}
