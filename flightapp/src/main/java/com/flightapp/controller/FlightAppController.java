package com.flightapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.flightapp.model.Flights;
import com.flightapp.service.FlightService;


@RestController
@RequestMapping("v1/api")
public class FlightAppController {

	@Autowired
	private FlightService flightService;
	
	
	@GetMapping("/flight/airline")
	public List<Flights> getFlights() {	
		return flightService.getAllFlights();
	}
	
	@PostMapping("/flight/airline/register")
	public Flights registerFlights(@RequestBody Flights flights) {	
		return flightService.registerFlights(flights);
	}
	
	@DeleteMapping("/flight/airline/remove/{id}")
	public void removeFlights(@PathVariable int id) {	
		 flightService.removeFlights(id);
	}
}
