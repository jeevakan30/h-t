package com.example.demo;

public class FlightDetails {

	private int id;
	private String airline;
	private String flightNumber;
	private int ticketCost;

	public FlightDetails() {

	}

	public FlightDetails(int id, String airline, String flightNumber, int ticketCost) {
		super();
		this.id = id;
		this.airline = airline;
		this.flightNumber = flightNumber;
		this.ticketCost = ticketCost;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public int getTicketCost() {
		return ticketCost;
	}

	public void setTicketCost(int ticketCost) {
		this.ticketCost = ticketCost;
	}

	@Override
	public String toString() {
		return "FlightDetails [id=" + id + ", airline=" + airline + ", flightNumber=" + flightNumber + ", ticketCost="
				+ ticketCost + "]";
	}

}
