package com.example.demo;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;

@Service
public class FlightLamda {

	private List<FlightDetails> flights = null;

	@PostConstruct
	public void setup() {
		flights = Arrays.asList(new FlightDetails(1, "Indigo", "IND76DS5", 4500),
				new FlightDetails(2, "KingFisher", "KIN76DS5", 5500),
				new FlightDetails(3, "SpiceJet", "SPI76DS5", 5603), 
				new FlightDetails(4, "AirIndia", "AIR76DS5", 5004)

		);
	}

//	@Bean
//	public List<FlightDetails> getFightsList() {
//
//		System.out.println("Finding All Flights " + LocalDateTime.now());
//		return flights;
//
//	}

	@Bean
	public Function<APIGatewayProxyRequestEvent, List<FlightDetails>> getAllFlights() {
		Function<APIGatewayProxyRequestEvent, List<FlightDetails>> f = (event) -> {
			return flights;
		};
		return f;

	}
}
